package ru.akarapetov.smpp;

import ie.omk.smpp.Address;
import ie.omk.smpp.BadCommandIDException;
import ie.omk.smpp.Connection;
import ie.omk.smpp.InvalidOperationException;
import ie.omk.smpp.message.*;
import ie.omk.smpp.message.tlv.Tag;
import org.apache.log4j.Logger;
import ru.akarapetov.smpp.model.SMPPConfig;
import ru.akarapetov.smpp.model.TLVField;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

/**
 * akarapetov
 * ru.akarapetov.smpp
 * java-smpp-client
 * 20.01.2016
 */
public class SMPPConnection {

    private Logger logger = Logger.getLogger(this.getClass());

    private Connection myConnection;

    private SMPPConfig smppConfig;

    public SMPPConnection(SMPPConfig smppConfig) {
        this.smppConfig = smppConfig;
    }

    public void bind() {
        String host = smppConfig.getHost();
        Integer port = smppConfig.getPort();

        int bindType = smppConfig.getBindType();
        String systemID = smppConfig.getSystemID();
        String password = smppConfig.getPassword();
        String systemType = smppConfig.getSystemType();
        int sourceTON = smppConfig.getSourceTON();
        int sourceNPI = smppConfig.getSourceNPI();
        String addressRange = smppConfig.getAddressRange();
        try {
            myConnection = new Connection(host, port);
            myConnection.autoAckLink(true);
            myConnection.autoAckMessages(true);

            logger.info("Binding to the SMSC");

            BindResp resp = myConnection.bind(
                    bindType,
                    systemID,
                    password,
                    systemType,
                    sourceTON,
                    sourceNPI,
                    addressRange);

            if (resp.getCommandStatus() != 0) {
                logger.info("SMSC bind failed.");
                System.exit(1);
            }

            logger.info("Bind successful");
        } catch (Exception x) {
            logger.info("An exception occurred.");
            x.printStackTrace(System.err);
        }
    }

    public void submitSM() {
        String message = smppConfig.getMessage();
        String address = smppConfig.getAddress();
        List<TLVField> tlvFieldList = smppConfig.getTlvFieldList();
        // Submit a simple message
        SubmitSM sm;
        try {
            sm = (SubmitSM) myConnection.newInstance(SMPPPacket.SUBMIT_SM);
        } catch (BadCommandIDException e) {
            e.printStackTrace();
            logger.error("BadCommandID");
            return;
        }
        sm.setDestination(new Address(0, 0, address));
        sm.setMessageText(message);

        for (TLVField tlvField : tlvFieldList) {
            Tag tag = tlvField.getTag();
            Object value = tlvField.getValue();
            sm.setOptionalParameter(tag, value);
        }

        SubmitSMResp smr = null;
        try {
            smr = (SubmitSMResp) myConnection.sendRequest(sm);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Error when send request");
        }

        logger.info("Submitted message ID: " + smr.getMessageId());

        try {
            // Wait a while, see if the SMSC delivers anything to us...
            SMPPPacket p = myConnection.readNextPacket();
            logger.info("Received a packet!");
            logger.info(p.toString());

            // API should be automatically acking deliver_sm and
            // enquire_link packets...
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (InvalidOperationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unbind() {
        // Unbind.
        UnbindResp ubr;
        try {
            ubr = myConnection.unbind();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Error unbind. Exit.");
            return;
        }

        if (ubr.getCommandStatus() == 0) {
            logger.info("Successfully unbound from the SMSC");
        } else {
            logger.info("There was an error unbinding.");
        }
    }
}
