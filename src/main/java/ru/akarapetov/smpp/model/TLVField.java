package ru.akarapetov.smpp.model;

import ie.omk.smpp.message.tlv.Tag;

/**
 * akarapetov
 * ru.akarapetov.smpp
 * java-smpp-client
 * 20.01.2016
 */
public class TLVField {
    private Tag tag;
    private Object value;
    private String clazz;

    public TLVField(Tag tag, Object value, String clazz) {
        this.tag = tag;
        this.value = value;
        this.clazz = clazz;
    }

    public Tag getTag() {
        return tag;
    }

    public Object getValue() {
        return value;
    }

    public String getClazz() {
        return clazz;
    }
}
