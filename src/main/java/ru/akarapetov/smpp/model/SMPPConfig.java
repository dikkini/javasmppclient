package ru.akarapetov.smpp.model;

import java.util.List;

/**
 * akarapetov
 * ru.akarapetov
 * java-smpp-client
 * 20.01.2016
 */
public class SMPPConfig {

    private String host;
    private Integer port;
    private String systemID;
    private String password;
    private String systemType;
    private int sourceTON;
    private int sourceNPI;
    private String addressRange;
    private int bindType;
    private List<TLVField> tlvFieldList;

    private String message;
    private String address;

    public SMPPConfig(String host, Integer port, String systemID, String password, String systemType, int sourceTON,
                      int sourceNPI, String addressRange, int bindType, List<TLVField> tlvFieldList, String message, String address) {
        this.host = host;
        this.port = port;
        this.systemID = systemID;
        this.password = password;
        this.systemType = systemType;
        this.sourceTON = sourceTON;
        this.sourceNPI = sourceNPI;
        this.addressRange = addressRange;
        this.bindType = bindType;
        this.tlvFieldList = tlvFieldList;
        this.message = message;
        this.address = address;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getSystemID() {
        return systemID;
    }

    public String getPassword() {
        return password;
    }

    public String getSystemType() {
        return systemType;
    }

    public int getSourceTON() {
        return sourceTON;
    }

    public int getSourceNPI() {
        return sourceNPI;
    }

    public String getAddressRange() {
        return addressRange;
    }

    public int getBindType() {
        return bindType;
    }

    public List<TLVField> getTlvFieldList() {
        return tlvFieldList;
    }

    public String getMessage() {
        return message;
    }

    public String getAddress() {
        return address;
    }
}
