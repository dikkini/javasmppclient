package ru.akarapetov;

import ie.omk.smpp.message.tlv.Tag;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Logger;
import ru.akarapetov.smpp.SMPPConnection;
import ru.akarapetov.smpp.model.SMPPConfig;
import ru.akarapetov.smpp.model.TLVField;

import java.util.ArrayList;
import java.util.List;

public class Application {

    private static Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {
        SMPPConfig smppConfig;
        try {
            smppConfig = readConfig();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error(e);
            return;
        } catch (ConfigurationException e) {
            e.printStackTrace();
            logger.error(e);
            return;
        }

        SMPPConnection smppConnection = new SMPPConnection(smppConfig);

        logger.info("Bind...");
        smppConnection.bind();

        logger.info("SubmitSM..");
        smppConnection.submitSM();

        logger.info("Unbind..");
        smppConnection.unbind();
    }

    private static SMPPConfig readConfig() throws ClassNotFoundException, ConfigurationException {
        logger.info("Read configuration file");
        XMLConfiguration config = new XMLConfiguration("./config.xml");

        logger.info("Read Bind section");
        SubnodeConfiguration smpp = config.configurationAt("bind");

        String host = smpp.getString("host");
        Integer port = smpp.getInt("port");
        String systemId = smpp.getString("systemId");
        String password = smpp.getString("password");
        String systemType = smpp.getString("systemType");
        Integer sourceTON = smpp.getInt("sourceTON");
        Integer sourceNPI = smpp.getInt("sourceNPI");
        String addressRange = smpp.getString("addressRange");
        Integer bindType = smpp.getInt("bindType");

        logger.info("Configuration SMPP");
        logger.info("Host : " + host);
        logger.info("Port : " + port);
        logger.info("SystemID : " + systemId);
        logger.info("Password : " + password);
        logger.info("SystemType : " + systemType);
        logger.info("SourceTON : " + sourceTON);
        logger.info("SourceNPI : " + sourceNPI);
        logger.info("AddressRange : " + addressRange);
        logger.info("BindType : " + bindType);

        logger.info("Read SubmitSM section");
        SubnodeConfiguration submitSm = config.configurationAt("submit_sm");

        String message = submitSm.getString("message");
        String address = submitSm.getString("address");

        logger.info("SubmitSM configuration");
        logger.info("Message : " + message);
        logger.info("Address : " + address);

        logger.info("Read TLV fields section");
        List<TLVField> tlvFieldList = new ArrayList<TLVField>();
        List<HierarchicalConfiguration> tlvFields = config.configurationsAt("submit_sm.tlv_fields.tlv");
        for (HierarchicalConfiguration tlvField: tlvFields) {
            Tag tag;

            Integer tagValue = tlvField.getInteger("tagValue", 0);
            String clazz = tlvField.getString("class");

            Object value;
            if (clazz.equals(String.class.getName())) {
                value = tlvField.getString("value");
                Integer minSize = tlvField.getInt("min_size");
                Integer maxSize = tlvField.getInt("max_size");
                tag = Tag.defineTag(tagValue, String.class, null, minSize, maxSize);
            } else if (clazz.equals(Integer.class.getName())) {
                value = tlvField.getInt("value");
                Integer fixedSize = tlvField.getInt("fixed_size");
                tag = Tag.defineTag(tagValue, Integer.class, null, fixedSize);
            } else if (clazz.equals(Long.class.getName())) {
                value = tlvField.getLong("value");
                Integer fixedSize = tlvField.getInt("fixed_size");
                tag = Tag.defineTag(tagValue, Long.class, null, fixedSize);
            } else {
                throw new IllegalArgumentException("Wrong class value for TLV field with tagValue=" + tagValue);
            }

            TLVField tlv = new TLVField(tag, value, clazz);
            tlvFieldList.add(tlv);
        }

        return new SMPPConfig(host, port, systemId, password, systemType, sourceTON, sourceNPI, addressRange, bindType,
                tlvFieldList, message, address);
    }
}
